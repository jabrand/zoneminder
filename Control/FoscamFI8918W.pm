# ==========================================================================
#
# ZoneMinder Foscam FI8918W IP Control Protocol Module, $Date: 2009-11-25 09:20:00 +0000 (Wed, 04 Nov 2009) $, $Revision: 0001 $
# Copyright (C) 2001-2008 Philip Coombes
# Modified for use with Foscam FI8918W IP Camera by Justin Mandzik
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA.
#
# ==========================================================================
#
# This module contains the implementation of the Foscam FI8918W IP camera control
# protocol
#
package ZoneMinder::Control::FoscamFI8918W;
use 5.006;
use strict;
use warnings;
require ZoneMinder::Base;
require ZoneMinder::Control;
our @ISA = qw(ZoneMinder::Control);
our $VERSION = $ZoneMinder::Base::VERSION;
# ==========================================================================
#
# Foscam FI8918W IP Control Protocol
#
# On ControlAddress use the format :
# USERNAME:PASSWORD@ADDRESS:PORT
# eg : admin:@10.1.2.1:80
# zoneminder:zonepass@10.0.100.1:40000
#
# ==========================================================================
use ZoneMinder::Logger qw(:all);
use ZoneMinder::Config qw(:all);
use Time::HiRes qw( usleep );
sub new
{
my $class = shift;
my $id = shift;
my $self = ZoneMinder::Control->new( $id );
my $logindetails = "";
bless( $self, $class );
srand( time() );
return $self;
}
our $AUTOLOAD;
sub AUTOLOAD
{
my $self = shift;
my $class = ref($self) || croak( "$self not object" );
my $name = $AUTOLOAD;
$name =~ s/.*://;
if ( exists($self->{$name}) )
{
return( $self->{$name} );
}
Fatal( "Can't access $name member of object of class $class" );
}
our $stop_command;
sub open
{
my $self = shift;
$self->loadMonitor();
use LWP::UserAgent;
$self->{ua} = LWP::UserAgent->new;
$self->{ua}->agent( "ZoneMinder Control Agent/".ZM_VERSION );
$self->{state} = 'open';
}
sub close
{
my $self = shift;
$self->{state} = 'closed';
}
sub printMsg
{
my $self = shift;
my $msg = shift;
my $msg_len = length($msg);
Debug( $msg."[".$msg_len."]" );
}
sub sendCmd
{
my $self = shift;
my $cmd = shift;
my $result = undef;
printMsg( $cmd, "Tx" );
my $req = HTTP::Request->new( GET=>"http://".$self->{Monitor}->{ControlAddress}."/$cmd" );
printMsg("GET http://".$self->{Monitor}->{ControlAddress}."/$cmd" );
my $res = $self->{ua}->request($req);
if ( $res->is_success )
{
$result = !undef;
}
else
{
Error( "Error check failed:'".$res->status_line()."'" );
}
return( $result );
}
sub reset
{
my $self = shift;
Debug( "Camera Reset" );
my $cmd = "reboot.cgi?user=admin&pwd=rain4est";
$self->sendCmd( $cmd );
}
#Up Arrow
sub moveConUp
{
my $self = shift;
my $params = shift;
$stop_command = "1";
Debug( "Move Up" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=0";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Down Arrow
sub moveConDown
{
my $self = shift;
my $params = shift;
$stop_command = "3";
Debug( "Move Down" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=2";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Left Arrow
sub moveConLeft
{
my $self = shift;
my $params = shift;
$stop_command = "5";
Debug( "Move Left" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=4";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Right Arrow
sub moveConRight
{
my $self = shift;
my $params = shift;
$stop_command = "7";
Debug( "Move Right" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=6";
$self->sendCmd( $cmd );
while (my ($k, $v) = each %$params ) {
Debug("$k, $v");
}
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Diagonally Up Right Arrow
sub moveConUpRight
{
my $self = shift;
my $params = shift;
$stop_command = "1";
Debug( "Move Diagonally Up Right" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=91";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Diagonally Down Right Arrow
sub moveConDownRight
{
my $self = shift;
my $params = shift;
$stop_command = "1";
Debug( "Move Diagonally Down Right" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=93";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Diagonally Up Left Arrow
sub moveConUpLeft
{
my $self = shift;
my $params = shift;
$stop_command = "1";
Debug( "Move Diagonally Up Left" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=90";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Diagonally Down Left Arrow
sub moveConDownLeft
{
my $self = shift;
my $params = shift;
$stop_command = "1";
Debug( "Move Diagonally Down Left" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=92";
$self->sendCmd( $cmd );
$self->autoStop( $stop_command, $self->{Monitor}->{AutoStopTimeout} );
}
#Stop
sub moveStop
{
my $self = shift;
Debug( "Move Stop" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=1";
$self->sendCmd( $cmd );
}
#autoStop
sub autoStop
{
my $self = shift;
my $stop_command = shift;
my $autostop = shift;
if( $autostop && $self->{Monitor}->{AutoStopTimeout} )
{
Debug( "Auto Stop" );
usleep( $self->{Monitor}->{AutoStopTimeout} );
#my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=".$stop_command;
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=1";
$self->sendCmd( $cmd );
}
}
#Move Camera to Home Position(Center)
sub presetHome
{
my $self = shift;
Debug( "Home Preset" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=25";
$self->sendCmd( $cmd );
}
#Horizontal Patrol
sub horizontalPatrol
{
my $self = shift;
Debug( "Horizontal Patrol" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=28";
$self->sendCmd( $cmd );
}
#Horizontal Patrol Stop
sub horizontalPatrolStop
{
my $self = shift;
Debug( "Horizontal Patrol Stop" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=29";
$self->sendCmd( $cmd );
}
#Vertical Patrol
sub verticalPatrol
{
my $self = shift;
Debug( "Vertical Patrol" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4est&command=26";
$self->sendCmd( $cmd );
}
#Vertical Patrol Stop
sub verticalPatrolStop
{
my $self = shift;
Debug( "Vertical Patrol Stop" );
my $cmd = "decoder_control.cgi?user=admin&pwd=rain4estcommand=27";
$self->sendCmd( $cmd );
}
1;
